<?php


namespace Tests\Feature\Domains\Playlists\Actions;


use Database\Factories\UserFactory;
use Domains\Playlists\Actions\CreateArtistAction;
use Domains\Playlists\Actions\CreateSongAction;
use Domains\Playlists\DTOs\CreateArtistDTO;
use Domains\Playlists\DTOs\CreateSongDTO;
use Domains\Playlists\Factories\ArtistFactory;
use Domains\Playlists\Models\Artist;
use Domains\Playlists\Models\Song;
use Domains\Playlists\Models\ThirdPartyAccess;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CreateSongActionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function song_can_be_created()
    {
        /** @var Song $artist */
        $song = CreateSongAction::run(new CreateSongDTO([
            'name' => 'Schrei nach Liebe',
            'image' => 'http://image.de',
            'duration' => '3:23',
            'artists' => ['Callejon'],
            'spotify_id' => 'spotifyID',
            'deezer_id' => 'deezerID',
        ]));

        $this->assertEquals('Schrei nach Liebe', $song->name);
        $this->assertEquals('http://image.de', $song->image);
        $this->assertEquals('3:23', $song->duration);
        $this->assertEquals('spotifyID', $song->spotify_id);
        $this->assertEquals('deezerID', $song->deezer_id);

        $this->assertEquals(['Callejon'], $song->artists);
    }
}
