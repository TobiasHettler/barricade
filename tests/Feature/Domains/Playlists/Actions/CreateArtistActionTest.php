<?php


namespace Tests\Feature\Domains\Playlists\Actions;


use Database\Factories\UserFactory;
use Domains\Playlists\Actions\CreateArtistAction;
use Domains\Playlists\Actions\CreateSongAction;
use Domains\Playlists\DTOs\CreateArtistDTO;
use Domains\Playlists\Models\Artist;
use Domains\Playlists\Models\ThirdPartyAccess;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CreateArtistActionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function artist_can_be_created()
    {

        /** @var Artist $artist */
        $artist = CreateArtistAction::run(new CreateArtistDTO([
            'name' => 'Callejon',
            'image' => 'http://image.de',
            'spotify_id' => 'spotifyID',
            'deezer_id' => 'deezerID',
        ]));

        $this->assertEquals('Callejon', $artist->name);
        $this->assertEquals('http://image.de', $artist->image);
        $this->assertEquals('spotifyID', $artist->spotify_id);
        $this->assertEquals('deezerID', $artist->deezer_id);
    }
}
