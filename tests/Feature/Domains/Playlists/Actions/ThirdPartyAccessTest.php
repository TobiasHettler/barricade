<?php


namespace Tests\Feature\Domains\Playlists\Actions;


use Database\Factories\UserFactory;
use Domains\Playlists\Models\ThirdPartyAccess;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ThirdPartyAccessTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function third_party_access_can_be_created()
    {
        $user = UserFactory::new()
            ->withPersonalTeam()
            ->create();
        $this->actingAs($user);

        $access = ThirdPartyAccess::create(
            [
                'team_id' => $user->currentTeam->id,
                'third_party' => 'spotify',
                'scopes' => ['scope'],
                'access_token' => 'accessToken',
                'refresh_token' => 'refreshToken',
            ]
        );

        $this->assertEquals($user->currentTeam->id, $access->team_id);
        $this->assertEquals('spotify', $access->third_party);
        $this->assertEquals(['scope'], $access->scopes);
        $this->assertEquals('accessToken', $access->access_token);
        $this->assertEquals('refreshToken', $access->refresh_token);
    }
}
