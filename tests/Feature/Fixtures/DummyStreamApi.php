<?php


namespace Tests\Feature\Fixtures;


use Domains\Playlists\DTOs\CreatePlaylistDTO;
use Domains\Playlists\DTOs\PlaylistData;
use Domains\Playlists\DTOs\SearchResultsCollection;
use Domains\Playlists\DTOs\SearchStreamingDTO;
use Domains\Playlists\DTOs\StreamingAccountData;
use Domains\Playlists\DTOs\UpdatePlaylistDTO;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;
use Domains\Playlists\Interfaces\StreamingApiInterface;

class DummyStreamApi implements StreamingApiInterface
{

    public function me(): StreamingAccountData
    {
        return new StreamingAccountData([
            'id' => 'account_id',
            'name' => 'Test Account',
            'followerCount' => 666,
           'imageHref' => 'pic.jpg',
           'link' => 'dummy.de/test',
        ]);
    }

    public function createPlaylist(CreatePlaylistDTO $dto): PlaylistData
    {
        return new PlaylistData([
            'id' => 'playlist_id',
            'name' => $dto->name,
            'description' => $dto->description,
            'public' => $dto->public,
            'follower_count' => 666,
            'img' => 'pic.jpg',
            'link' => 'dummy.de/test',
        ]);
    }

    public function updatePlaylist(UpdatePlaylistOnStreamingDTO $dto): PlaylistData
    {
        return new PlaylistData([
            'id' => $dto->id,
            'name' => $dto->name,
            'description' => $dto->description,
            'public' => $dto->public,
            'follower_count' => 666,
            'img' => 'pic.jpg',
            'link' => 'dummy.de/test',
        ]);
    }

    public function search(SearchStreamingDTO $dto): SearchResultsCollection
    {
        // TODO: Implement search() method.
    }
}
