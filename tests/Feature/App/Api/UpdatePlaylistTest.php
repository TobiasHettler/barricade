<?php


namespace Tests\Feature\App\Api;

use Database\Factories\UserFactory;
use Domains\Playlists\Factories\PlaylistFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Tests\Feature\Fixtures\DummyStreamApi;
use Tests\TestCase;

class UpdatePlaylistTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('playlists.drivers.spotify.api', DummyStreamApi::class);
        Config::set('playlists.drivers.deezer.api', DummyStreamApi::class);

        $this->actingAs(UserFactory::new()->withPersonalTeam()->create());
    }

    /** @test */
    public function playlists_can_be_updated_changes_will_be_synced()
    {
        $playlist = PlaylistFactory::new()->withSpotify()->withDeezer()->create();

        $this->json('put', '/api/playlists/' . $playlist->id, [
            'name' => 'Updated Test Playlist',
            'spotify_description' => 'Updated Spotify Description',
            'deezer_description' => 'Updated Deezer Description',
            'deezer_id' => 'updatedDeezerId',
            'spotify_id' => 'updatedSpotifyId',
            'public' => false,
        ])
            ->assertStatus(200)
            ->assertJsonFragment([
                'id' => 1,
                'name' => 'Updated Test Playlist',
                'public' => false,
                'spotify' => [
                    'id' => 'updatedSpotifyId',
                    'description' => 'Updated Spotify Description',
                    'name' => 'Updated Test Playlist',
                    'link' => 'dummy.de/test',
                    'img' => 'pic.jpg',
                    'follower_count' => 666,
                    'public' => false
                ],
                'deezer' => [
                    'id' => 'updatedDeezerId',
                    'description' => 'Updated Deezer Description',
                    'name' => 'Updated Test Playlist',
                    'link' => 'dummy.de/test',
                    'img' => 'pic.jpg',
                    'follower_count' => 666,
                    'public' => false
                ],
            ]);
    }

    /** @test */
    public function playlists_can_be_updated_changes_will_not_be_synced_it_no_streaming_ids_exist()
    {
        $playlist = PlaylistFactory::new()->create();

        $this->json('put', '/api/playlists/' . $playlist->id, [
            'name' => 'Updated Test Playlist',
            'spotify_description' => 'Updated Spotify Description',
            'deezer_description' => 'Updated Deezer Description',
            'deezer_id' => null,
            'spotify_id' => null,
            'public' => false,
        ])
            ->assertStatus(200)
            ->assertJsonFragment([
                'id' => 1,
                'name' => 'Updated Test Playlist',
                'public' => false,
                'spotify' => [],
                'deezer' => [],
            ]);
    }
}
