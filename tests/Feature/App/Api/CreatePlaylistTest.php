<?php


namespace Tests\Feature\App\Api;


use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Tests\Feature\Fixtures\DummyStreamApi;
use Tests\TestCase;

class CreatePlaylistTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function playlists_can_be_created()
    {
        $this->actingAs(UserFactory::new()->withPersonalTeam()->create());
        Config::set('playlists.drivers.spotify.api', DummyStreamApi::class);
        Config::set('playlists.drivers.deezer.api', DummyStreamApi::class);

        $this->assertDatabaseCount('playlists', 0);

        $this->post('/api/playlists', [
            'name' => 'Test Playlist',
            'spotify_description' => 'Spotify Description',
            'deezer_description' => 'Deezer Description',
            'public' => true,
        ])
            ->assertStatus(201)
            ->assertJsonFragment([
                'id' => 1,
                'name' => 'Test Playlist',
                'public' => true,
                'spotify' => [
                    'id' => 'playlist_id',
                    'description' => 'Spotify Description',
                    'name' => 'Test Playlist',
                    'link' => 'dummy.de/test',
                    'img' => 'pic.jpg',
                    'follower_count' => 666,
                    'public' => true
                ],
                'deezer' => [
                    'id' => 'playlist_id',
                    'description' => 'Deezer Description',
                    'name' => 'Test Playlist',
                    'link' => 'dummy.de/test',
                    'img' => 'pic.jpg',
                    'follower_count' => 666,
                    'public' => true
                ],
            ]);

        $this->assertDatabaseCount('playlists', 1);
    }

    /** @test */
    public function playlists_can_be_created_without_syncing_to_spotify()
    {
        $this->actingAs(UserFactory::new()->withPersonalTeam()->create());
        Config::set('playlists.drivers.spotify.api', DummyStreamApi::class);
        Config::set('playlists.drivers.deezer.api', DummyStreamApi::class);

        $this->post('/api/playlists', [
            'name' => 'Test Playlist',
            'spotify_description' => 'Spotify Description',
            'deezer_description' => 'Deezer Description',
            'public' => true,
            'sync_spotify' => false,
            'sync_deezer' => true,
        ])
            ->assertStatus(201)
            ->assertJsonFragment([
                'id' => 1,
                'name' => 'Test Playlist',
                'public' => true,
                'spotify' => [],
                'deezer' => [
                    'id' => 'playlist_id',
                    'description' => 'Deezer Description',
                    'name' => 'Test Playlist',
                    'link' => 'dummy.de/test',
                    'img' => 'pic.jpg',
                    'follower_count' => 666,
                    'public' => true
                ],
            ]);


    }


    /** @test */
    public function playlists_can_be_created_without_syncing_to_deezer()
    {
        $this->actingAs(UserFactory::new()->withPersonalTeam()->create());
        Config::set('playlists.drivers.spotify.api', DummyStreamApi::class);
        Config::set('playlists.drivers.deezer.api', DummyStreamApi::class);

        $this->post('/api/playlists', [
            'name' => 'Test Playlist',
            'spotify_description' => 'Spotify Description',
            'deezer_description' => 'Deezer Description',
            'public' => true,
            'sync_deezer' => false,
            'sync_spotify' => true
        ])
            ->assertStatus(201)
            ->assertJsonFragment([
                'id' => 1,
                'name' => 'Test Playlist',
                'public' => true,
                'spotify' => [
                    'id' => 'playlist_id',
                    'description' => 'Spotify Description',
                    'name' => 'Test Playlist',
                    'link' => 'dummy.de/test',
                    'img' => 'pic.jpg',
                    'follower_count' => 666,
                    'public' => true
                ],
                'deezer' => [],
            ]);


    }
}
