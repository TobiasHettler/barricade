<?php


namespace Tests\Feature\App\Api;


use Database\Factories\UserFactory;
use Domains\Playlists\Factories\PlaylistFactory;
use Domains\Playlists\Models\Playlist;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Tests\Feature\Fixtures\DummyStreamApi;
use Tests\TestCase;

class AddSongToPlaylistsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function song_can_be_added_to_playlists()
    {
        $this->actingAs(UserFactory::new()->withPersonalTeam()->create());
        Config::set('playlists.drivers.spotify.api', DummyStreamApi::class);
        Config::set('playlists.drivers.deezer.api', DummyStreamApi::class);

        $playlists = PlaylistFactory::new()->count(2)->create();

        $this->json('post','/api/playlists/add-song', [
            'playlists' => $playlists->map(fn(Playlist $playlist) => $playlist->id)->all(),
            'song' => [
                    'deezer_id' => '750592362',
                    'spotify_id' => 'sdiw0202h',
                    'name' => 'Sneak Preview',
                    'image' => 'https://api.deezer.com/album/110480432/image',
                    'duration' => '3:46',
                    'artists' => [
                        'ASD',
                        'Callejon'
                    ],
                ]
            ])
            ->assertStatus(201);

        $playlists->each(function (Playlist $playlist) {
            $this->assertCount(1, $playlist->songs);
            $this->assertEquals('Sneak Preview', $playlist->songs->first()->name);
        });
    }

    /** @test */
    public function playlists_can_be_created_without_syncing_to_spotify()
    {
        $this->actingAs(UserFactory::new()->withPersonalTeam()->create());
        Config::set('playlists.drivers.spotify.api', DummyStreamApi::class);
        Config::set('playlists.drivers.deezer.api', DummyStreamApi::class);

        $this->post('/api/playlists', [
            'name' => 'Test Playlist',
            'spotify_description' => 'Spotify Description',
            'deezer_description' => 'Deezer Description',
            'public' => true,
            'sync_spotify' => false,
            'sync_deezer' => true,
        ])
            ->assertStatus(201)
            ->assertJsonFragment([
                'id' => 1,
                'name' => 'Test Playlist',
                'public' => true,
                'spotify' => [],
                'deezer' => [
                    'id' => 'playlist_id',
                    'description' => 'Deezer Description',
                    'name' => 'Test Playlist',
                    'link' => 'dummy.de/test',
                    'img' => 'pic.jpg',
                    'follower_count' => 666,
                    'public' => true
                ],
            ]);


    }


    /** @test */
    public function playlists_can_be_created_without_syncing_to_deezer()
    {
        $this->actingAs(UserFactory::new()->withPersonalTeam()->create());
        Config::set('playlists.drivers.spotify.api', DummyStreamApi::class);
        Config::set('playlists.drivers.deezer.api', DummyStreamApi::class);

        $this->post('/api/playlists', [
            'name' => 'Test Playlist',
            'spotify_description' => 'Spotify Description',
            'deezer_description' => 'Deezer Description',
            'public' => true,
            'sync_deezer' => false,
            'sync_spotify' => true
        ])
            ->assertStatus(201)
            ->assertJsonFragment([
                'id' => 1,
                'name' => 'Test Playlist',
                'public' => true,
                'spotify' => [
                    'id' => 'playlist_id',
                    'description' => 'Spotify Description',
                    'name' => 'Test Playlist',
                    'link' => 'dummy.de/test',
                    'img' => 'pic.jpg',
                    'follower_count' => 666,
                    'public' => true
                ],
                'deezer' => [],
            ]);


    }
}
