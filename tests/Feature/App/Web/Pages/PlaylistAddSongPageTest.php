<?php


namespace Tests\Feature\App\Web\Pages;


use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlaylistAddSongPageTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function playlist_add_song_screen_can_be_rendered()
    {
        $this->actingAs(
            UserFactory::new()
            ->withPersonalTeam()
            ->create()
        );

        $response = $this->get('/playlists/add-song');
        $response->assertStatus(200);
    }

}
