<?php


namespace Tests\Feature\App\Web\Pages;


use Database\Factories\UserFactory;
use Domains\Playlists\Factories\PlaylistFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlaylistEditPageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function playlist_edit_screen_can_be_rendered()
    {
        $this->actingAs(
            UserFactory::new()
                ->withPersonalTeam()
                ->create()
        );

        $playlist = PlaylistFactory::new()->create();

        $response = $this->get('/playlists/' . $playlist->id . '/edit');
        $response->assertStatus(200);
    }

}
