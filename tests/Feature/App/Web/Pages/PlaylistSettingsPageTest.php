<?php


namespace Tests\Feature\App\Web\Pages;


use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlaylistSettingsPageTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function playlist_settings_screen_can_be_rendered()
    {
        $this->actingAs(
            UserFactory::new()
            ->withPersonalTeam()
            ->create()
        );
        $response = $this->get('/playlists/settings');
        $response->assertStatus(200);
    }

}
