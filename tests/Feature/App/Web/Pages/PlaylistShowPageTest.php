<?php


namespace Tests\Feature\App\Web\Pages;


use Database\Factories\UserFactory;
use Domains\Playlists\Factories\PlaylistFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlaylistShowPageTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function playlist_settings_screen_can_be_rendered()
    {
        $this->actingAs(
            UserFactory::new()
            ->withPersonalTeam()
            ->create()
        );
        $playlist = PlaylistFactory::new()->create();

        $response = $this->get('/playlists/' . $playlist->id);
        $response->assertStatus(200);
    }

}
