<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('/playlists', \App\Api\Controllers\CreatePlaylistController::class)
    ->name( 'api.playlists.create');

    Route::post('/playlists/add-song', \Domains\Playlists\Actions\AddSongToPlaylistsAction::class)
        ->name( 'api.playlists.add-song');

    Route::put('/playlists/{playlist}', \Domains\Playlists\Actions\UpdatePlaylistAction::class)
    ->name('api.playlists.update');

    Route::get('/streaming/{driver}/me', \App\Api\Controllers\GetStreamingAccountController::class);

    Route::get('/streaming/{driver}/search', \Domains\Playlists\Actions\SearchStreamingAction::class)
    ->name('streaming.search');
});
