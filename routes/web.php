<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});


Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/dashboard', fn() => Inertia::render('Dashboard'))
        ->name('dashboard');

    Route::prefix('/playlists')->group(function () {
        Route::get('/', \App\Web\Controllers\Pages\PlaylistOverviewPageController::class)
            ->name('playlists');

        Route::get('/add-song', fn () => Inertia::render('Playlists/AddSong'))
            ->name('playlists.add-song');

        Route::get('/settings', fn () => Inertia::render('Playlists/Settings'))
            ->name('playlists.settings');

        Route::get('/suggestions', \App\Web\Controllers\Pages\PlaylistOverviewPageController::class)
            ->name('playlists.suggestions');

        Route::get('/create', fn () => Inertia::render('Playlists/Create'))
            ->name('playlists.create');

        Route::get('/add-song', \App\Web\Controllers\Pages\PlaylistController::at('addSong'))
            ->name('playlists.add-song');

        Route::get('/{playlist}', \App\Web\Controllers\Pages\PlaylistController::at('show'))
            ->name('playlists.show');

        Route::get('/{playlist}/edit', \App\Web\Controllers\Pages\PlaylistController::at('edit'))
            ->name('playlists.edit');

        Route::get('/{thirdParty}/authenticate', \App\Web\Controllers\Playlists\ConnectorController::at('authenticate'))
            ->name('playlists.third-party.auth');

        Route::get('/{thirdParty}/callback', \App\Web\Controllers\Playlists\ConnectorController::at('callback'))
            ->name('playlists.third-party.callback');
    });


    Route::put('/music/playlists', \Domains\Playlists\Actions\UpdateSpotifyPlaylistAction::class)
        ->name('music.playlists.update');

});


Route::get('/spotify/authenticate', \Domains\Playlists\Actions\ConnectToSpotifyAction::at('authenticate'))
    ->name('spotify.auth');
Route::get('/spotify/authenticate/callback', \Domains\Playlists\Actions\ConnectToSpotifyAction::at('callback'));
//Route::get('/spotify/me', \StageSlice\SpotifyAdmin\Actions\GetMeAction::class);


Route::get('/deezer/authenticate', \Domains\Playlists\Actions\ConnectToDeezerAction::at('authenticate'))
    ->name('deezer.auth');
Route::get('/deezer/authenticate/callback', \Domains\Playlists\Actions\ConnectToDeezerAction::at('callback'))
    ->name('deezer.auth.callback');
//Route::get('/spotify/me', \StageSlice\SpotifyAdmin\Actions\GetMeAction::class);
