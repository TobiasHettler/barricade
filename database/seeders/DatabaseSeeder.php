<?php

namespace Database\Seeders;

use Database\Factories\UserFactory;
use Domains\Playlists\Models\ThirdPartyAccess;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = UserFactory::new([
            'name' => 'admin',
            'password' => Hash::make('password'),
            'email' => 'admin@test.de',
        ])->withPersonalTeam()
            ->create();

        ThirdPartyAccess::create(
            [
                'team_id' => $user->currentTeam->id,
                'third_party' => 'spotify',
                'scopes' => ["playlist-read-private", "playlist-read-collaborative", "playlist-modify-private", "playlist-modify-public", "user-read-private"],
                'access_token' => 'BQBHKMdwcIXxysBkSG6YIg0h5_srH-eqcZwCzH8efwq6r80Lv8nBsJ6TnT41ZGaGxpENr2bH4J3U-WXDphCc2IXvbA0JmFDjA-BiK1b8dFCWV_xBJbDIAzYNLJNEBa5lrQJ0ZyIBiEOJ6K8SmLBC6InBYmx2eaeHV-jHkiUSvaBCW9ff9MuBPnMniNevOjtx6jHvJCbeF4YV08YD0bdnXtua3AB42JgYNrmNTYWaU5jDD3dZr5tpo28',
                'refresh_token' => 'AQB-OSekT6st-vSESFRJS7_loB4hGnXiP654Jrb2ngvCY_B6fWNIKkyRZSu3iSmb3c4tGM9LQaeHO3u7ftPAtZkpPP2LJ8xknyvZyO7mCJS3SMzEXhxvKbL6P1yK5a3YAtc',
            ]
        );

        ThirdPartyAccess::create(
            [
                'team_id' => $user->currentTeam->id,
                'third_party' => 'deezer',
                'scopes' => ["basic_access", "manage_library", "offline_access"],
                'access_token' => 'frdO7SPcQ7lHImrWXzmrscVjDzyKxLEQE0MVU03KxlbCNaP3ID',
                'refresh_token' => null,
            ]
        );
    }
}
