<?php
return [
    'enabled' => ['deezer', 'spotify'],
    'drivers' => [
        'spotify' => [
            'connector' => \Domains\Playlists\Drivers\Spotify\SpotifyConnector::class,
            'api' => \Domains\Playlists\Drivers\Spotify\SpotifyApi::class,
            'clientID' => env('SPOTIFY_CLIENT_ID'),
            'clientSecret' => env('SPOTIFY_CLIENT_SECRET'),
            'redirectUri' => env('SPOTIFY_REDIRECT_URI'),
            'scopes' => [
                'playlist-read-private',
                'playlist-modify-private',
                'playlist-modify-public',
                'playlist-read-collaborative',
                'user-read-private',
            ]
        ],
        'deezer' => [
            'connector' => \Domains\Playlists\Drivers\Deezer\DeezerConnector::class,
            'api' => \Domains\Playlists\Drivers\Deezer\DeezerApi::class,
            'clientID' => env('DEEZER_CLIENT_ID'),
            'clientSecret' => env('DEEZER_CLIENT_SECRET'),
            'redirectUri' => env('DEEZER_REDIRECT_URI'),
            'api_host' => 'https://api.deezer.com',
            'authUrl' => 'https://connect.deezer.com/oauth/auth.php',
            'accessUrl' => 'https://connect.deezer.com/oauth/access_token.php',
            'scopes' => [
                'basic_access',
                'manage_library',
                'offline_access'
            ],
        ]
    ]
];
