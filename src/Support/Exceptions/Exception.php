<?php


namespace Support\Exceptions;

class Exception extends \Exception
{
    public static function throw(string $message): void
    {
        throw new static($message);
    }
}
