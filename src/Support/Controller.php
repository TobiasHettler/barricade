<?php

namespace Support;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Use for Route registration
     *
     * @param  string  $action
     * @return array
     */
    public static function at(string $action): array
    {
        return [static::class, $action];
    }
}
