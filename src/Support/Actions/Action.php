<?php


namespace Support\Actions;

use Illuminate\Support\Facades\Validator;
use Spatie\DataTransferObject\DataTransferObject;

abstract class Action implements ActionInterface
{
    protected bool $force = false;

    public function __construct(
        protected array|DataTransferObject $data
    )
    {}

    public static function rules(): array
    {
        return [];
    }

    public function data(): array|DataTransferObject
    {
        return $this->data;
    }

    public function validate(): void
    {
        $validator = Validator::make(
            is_array($this->data()) ? $this->data() : $this->data()->toArray(),
            static::rules()
        );

        if ($validator->fails()) {
            ActionValidationException::throw($validator->getMessageBag());
        }
    }

    public function force(bool $force = true): self
    {
        $this->force = $force;
        return $this;
    }

    public function run(...$params): mixed
    {
        if (!$this->force){
            $this->validate();
        }
        return $this->handle(...$params);
    }
}
