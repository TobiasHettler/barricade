<?php

namespace Support\Actions;

interface ActionInterface
{
    public function handle(): mixed;

    public function run(): mixed;
}
