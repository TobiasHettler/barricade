<?php


namespace Domains\Playlists\Drivers\Deezer;


use Domains\Playlists\DTOs\CreateThirdPartyAccessDTO;
use Domains\Playlists\Interfaces\ConnectorInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DeezerConnector implements ConnectorInterface
{

    public function authorizeUrl(): string
    {
        $query = http_build_query([
            'app_id' => config('playlists.drivers.deezer.clientID'),
            'redirect_uri' => config('playlists.drivers.deezer.redirectUri'),
            'perms' => implode(',', config('playlists.drivers.deezer.scopes'))
        ]);

        return config('playlists.drivers.deezer.authUrl') . '?' . $query;
    }

    public function callback(Request $request): CreateThirdPartyAccessDTO
    {
        $response = Http::get(config('playlists.drivers.deezer.accessUrl'),[
            'app_id' => env('DEEZER_CLIENT_ID'),
            'secret' => env('DEEZER_CLIENT_SECRET'),
            'code' => $request->query('code')
        ])->body();

        parse_str($response, $result);

        return new CreateThirdPartyAccessDTO([
            'accessToken' => $result['access_token'],
            'refreshToken' => null,
            'thirdParty' => 'deezer',
            'scopes' => config('playlists.drivers.deezer.scopes'),
        ]);
    }

    public function refresh(): CreateThirdPartyAccessDTO
    {
        // TODO: Implement refresh() method.
    }
}
