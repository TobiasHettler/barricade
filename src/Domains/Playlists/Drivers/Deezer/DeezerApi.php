<?php


namespace Domains\Playlists\Drivers\Deezer;


use Domains\Playlists\DTOs\ArtistData;
use Domains\Playlists\DTOs\CreatePlaylistDTO;
use Domains\Playlists\DTOs\PlaylistData;
use Domains\Playlists\DTOs\SearchData;
use Domains\Playlists\DTOs\SearchResultsCollection;
use Domains\Playlists\DTOs\SearchStreamingDTO;
use Domains\Playlists\DTOs\StreamingAccountData;
use Domains\Playlists\DTOs\UpdatePlaylistDTO;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;
use Domains\Playlists\Interfaces\StreamingApiInterface;
use Domains\Playlists\Models\ThirdPartyAccess;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use SpotifyWebAPI\SpotifyWebAPI;

class DeezerApi implements StreamingApiInterface
{

    protected SpotifyWebAPI $spotifyWebApi;
    protected ThirdPartyAccess $deezerAccess;

    protected array $endpoints = [
        'me' => '/user/me',
        'followers' => '/user/me/followers',
        'createPlaylist' => '/user/me/playlists',
        'updatePlaylist' => '/playlist/$id',
        'getPlaylist' => '/playlist/$id',
        'searchTrack' => '/search/track',
    ];

    public function __construct()
    {
        $this->deezerAccess = ThirdPartyAccess::currentTeam('deezer');
    }

    public function me(): StreamingAccountData
    {
        $meResponse = $this->callGetDeezer($this->endpoints['me']);
        $followerResponse = $this->callGetDeezer($this->endpoints['followers']);

        return new StreamingAccountData([
            'id' => (string)$meResponse['id'],
            'name' => $meResponse['name'],
            'followerCount' => $followerResponse['total'],
            'imageHref' => $meResponse['picture'],
            'link' => $meResponse['link'],
        ]);
    }

    public function createPlaylist(CreatePlaylistDTO $dto): PlaylistData
    {
        ['id' => $id] = $this->callPostDeezer($this->endpoints['createPlaylist'], ['title' => $dto->name]);

        return $this->updatePlaylist(new UpdatePlaylistOnStreamingDTO([
            'id' => (string)$id,
            'description' => $dto->description,
            'public' => $dto->public,
        ]));
    }

    public function updatePlaylist(UpdatePlaylistOnStreamingDTO $dto): PlaylistData
    {
        $endpoint = Str::replaceFirst('$id', $dto->id, $this->endpoints['updatePlaylist']);

        $params = $dto->except('playlist_id')->all();
        $params['title'] = $params['name'];
        unset($params['name']);
        $params = array_filter($params, fn($value) => $value !== null);
        $params = array_map(fn($value) => is_bool($value) ? ($value ? 'true' : 'false') : $value, $params);

        $this->callPostDeezer($endpoint, $params);

        return $this->getPlaylist($dto->id);
    }

    public function getPlaylist(string $playlist_id): PlaylistData
    {
        $endpoint = Str::replaceFirst('$id', $playlist_id, $this->endpoints['getPlaylist']);
        $response = $this->callGetDeezer($endpoint);

        return new PlaylistData([
            'id' => (string)$response['id'],
            'name' => $response['title'],
            'description' => $response['description'],
            'public' => $response['public'],
            'follower_count' => $response['fans'],
            'img' => $response['picture'],
            'link' => $response['link'],
        ]);
    }

    protected function callGetDeezer(string $endpoint, array $params = []): array
    {
        return Http::get(
            config('playlists.drivers.deezer.api_host') . $endpoint,
            array_merge(
                ['access_token' => $this->deezerAccess->access_token],
                $params)
        )->json();
    }

    protected function callPostDeezer(string $endpoint, array $params = []): array
    {

        $endpoint = config('playlists.drivers.deezer.api_host') . $endpoint . '?access_token=' . $this->deezerAccess->access_token;
        return Arr::wrap(Http::asForm()
            ->post($endpoint, $params)
            ->json());
    }


    public function search(SearchStreamingDTO $dto): SearchResultsCollection
    {
        $response = $this->callGetDeezer($this->endpoints['searchTrack'], ['q' => $dto->query]);

        $result = collect($response['data'])->map(function ($item) {
            return new SearchData([
                'id' => (string) $item['id'],
                'name' => $item['title'],
                'type' => $item['type'],
                'duration' => $this->sToM($item['duration']),
                'image' => $item['album']['cover'],
                'artists' => [new ArtistData([
                    'id' =>  (string) $item['artist']['id'],
                    'name' =>  $item['artist']['name'],
                    'href' =>  $item['artist']['link'],
                    'image' =>  $item['artist']['picture'],
                ])],
            ]);
        });

        return new SearchResultsCollection($result->all());
    }

    protected function sToM(int $input)
    {
        $seconds = $input % 60;
        $input = floor($input / 60);

        $minutes = $input % 60;
        return floor($minutes) . ':' . ($seconds <= 9 ? '0'.$seconds : $seconds) ;
    }
}
