<?php


namespace Domains\Playlists\Drivers\Spotify;


use Domains\Playlists\DTOs\ArtistData;
use Domains\Playlists\DTOs\CreatePlaylistDTO;
use Domains\Playlists\DTOs\PlaylistData;
use Domains\Playlists\DTOs\SearchData;
use Domains\Playlists\DTOs\SearchResultsCollection;
use Domains\Playlists\DTOs\SearchStreamingDTO;
use Domains\Playlists\DTOs\StreamingAccountData;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;
use Domains\Playlists\Interfaces\StreamingApiInterface;
use Domains\Playlists\Models\ThirdPartyAccess;
use Domains\Playlists\ThirdPartyConnector;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SpotifyWebAPI\SpotifyWebAPI;
use SpotifyWebAPI\SpotifyWebAPIException;

class SpotifyApi implements StreamingApiInterface
{

    protected SpotifyWebAPI $spotifyWebApi;
    protected ThirdPartyAccess $spotifyAccess;

    public function __construct()
    {
        $this->spotifyAccess = ThirdPartyAccess::currentTeam('spotify');
        $this->spotifyWebApi = new SpotifyWebAPI();
        $this->spotifyWebApi->setAccessToken($this->spotifyAccess->access_token);
    }

    public function me(): StreamingAccountData
    {
        $response = $this->callSpotify(fn(SpotifyWebAPI $api) => $api->me());
        return new StreamingAccountData([
            'id' => $response->id,
            'name' => $response->display_name,
            'followerCount' => $response->followers->total,
            'imageHref' => $response->images[0]->url,
            'link' => $response->external_urls->spotify,
        ]);
    }

    public function createPlaylist(CreatePlaylistDTO $dto): PlaylistData
    {
        $response = $this->callSpotify(fn(SpotifyWebAPI $api) => $api->createPlaylist([
            'name' => $dto->name,
            'description' => $dto->description,
            'public' => $dto->public,
        ]));

        return new PlaylistData([
            'id' => $response->id,
            'name' => $response->name,
            'description' => $response->description,
            'public' => $response->public,
            'follower_count' => $response->followers->total,
            'img' => !empty($response->images) ? $response->images[0]->url : null,
            'link' => $response->external_urls->spotify,
        ]);
    }

    protected function callSpotify(\Closure $action)
    {
        try {
            return $action($this->spotifyWebApi);
        } catch (SpotifyWebAPIException $e) {
            try {
                (new ThirdPartyConnector('spotify'))->refresh();
                $this->setAccessToken($this->spotifyAccess->fresh()->access_token);

                return $action($this->spotifyWebApi);
            } catch (SpotifyWebAPIException $e) {
                Log::alert('Spotify: Token could not be refreshed [' . $e->getCode() . ' ' . $e->getMessage() . ']');
            }
        }
    }

    protected function setAccessToken(string $accessToken): self
    {
        $this->spotifyWebApi->setAccessToken($accessToken);
        return $this;
    }

    public function updatePlaylist(UpdatePlaylistOnStreamingDTO $dto): PlaylistData
    {
        $this->callSpotify(fn(SpotifyWebAPI $api) => $api->updatePlaylist($dto->id, [
            'name' => $dto->name,
            'description' => $dto->description,
            'public' => $dto->public,
        ]));

        return $this->getPlaylist($dto->id);
    }

    public function getPlaylist(string $playlist_id): PlaylistData
    {
        $response = $this->callSpotify(fn(SpotifyWebAPI $api) => $api->getPlaylist($playlist_id));

        return new PlaylistData([
            'id' => $response->id,
            'name' => $response->name,
            'description' => $response->description,
            'public' => $response->public,
            'follower_count' => $response->followers->total,
            'img' => !empty($response->images) ? $response->images[0]->url : null,
            'link' => $response->external_urls->spotify,
        ]);
    }

    public function search(SearchStreamingDTO $dto): SearchResultsCollection
    {
        $response = $this->callSpotify(fn(SpotifyWebAPI $api) => $api->search($dto->query, 'track', ['limit' => 10,'market' => 'from_token']));

        $result = collect($response->tracks->items)->map(function ($item) {
            return new SearchData([
                'id' => $item->id,
                'name' => $item->name,
                'type' => $item->type,
                'duration' => $this->msToM($item->duration_ms),
                'image' => Arr::last($item->album->images)->url,
                'artists' => array_map(fn($artist) => new ArtistData([
                    'id' => $artist->id,
                    'name' => $artist->name,
                    'href' => $artist->external_urls->spotify,
                ]), $item->artists),
            ]);
        });

        return new SearchResultsCollection($result->all());
    }

    protected function msToM(int $input)
    {
        $uSec = $input % 1000;
        $input = floor($input / 1000);

        $seconds = $input % 60;
        $input = floor($input / 60);

        $minutes = $input % 60;
        return floor($minutes) . ':' . ($seconds <= 9 ? '0'.$seconds : $seconds) ;
    }
}
