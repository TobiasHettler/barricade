<?php


namespace Domains\Playlists\Drivers\Spotify;


use Domains\Playlists\DTOs\CreateThirdPartyAccessDTO;
use Domains\Playlists\Exceptions\SpotifyConnectionException;
use Domains\Playlists\Interfaces\ConnectorInterface;
use Domains\Playlists\Models\ThirdPartyAccess;
use Illuminate\Http\Request;
use SpotifyWebAPI\Session;

class SpotifyConnector implements ConnectorInterface
{

    /**
     * @var \Illuminate\Contracts\Foundation\Application|mixed
     */
    protected Session $session;

    public function __construct()
    {
        $this->session = new Session(
            config('spotify.clientID'),
            config('spotify.clientSecret'),
            config('spotify.redirectUri'),
        );
    }

    public function authorizeUrl(): string
    {
        return $this->session->getAuthorizeUrl([
            'scope' => config('playlists.drivers.spotify.scopes')
        ]);
    }

    public function callback(Request $request): CreateThirdPartyAccessDTO
    {
        $code = $request->input('code', false);

        if (!$code) {
            throw SpotifyConnectionException::make('"code" is missing in Callback request.');
        }

        $this->session->requestAccessToken($code);

        return new CreateThirdPartyAccessDTO([
            'accessToken' => $this->session->getAccessToken(),
            'refreshToken' => $this->session->getRefreshToken(),
            'thirdParty' => 'spotify',
            'scopes' => $this->session->getScope(),
        ]);
    }

    public function refresh(): CreateThirdPartyAccessDTO
    {
        $this->session->refreshAccessToken(ThirdPartyAccess::currentTeam('spotify')->refresh_token);
        return new CreateThirdPartyAccessDTO([
            'accessToken' => $this->session->getAccessToken(),
            'refreshToken' => $this->session->getRefreshToken(),
            'thirdParty' => 'spotify',
            'scopes' => $this->session->getScope(),
        ]);
    }
}
