<?php


namespace Domains\Playlists\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Artist extends Model
{
    protected $guarded = [];

    public function songs(): BelongsToMany
    {
        return $this->belongsToMany(Song::class);
    }
}
