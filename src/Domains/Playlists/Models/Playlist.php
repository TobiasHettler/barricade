<?php


namespace Domains\Playlists\Models;

use App\Models\Team;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

class Playlist extends Model
{
    protected $guarded = [];
    protected $casts = [
        'public' => 'boolean',
        'spotify' => 'array',
        'deezer' => 'array'
    ];

    public function songs(): BelongsToMany
    {
        return $this->belongsToMany(Song::class);
    }
}
