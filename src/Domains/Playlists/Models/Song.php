<?php


namespace Domains\Playlists\Models;


use Illuminate\Database\Eloquent\Model;


class Song extends Model
{
    protected $guarded = [];
    protected $casts = [
        'artists' =>'array'
    ];
}
