<?php


namespace Domains\Playlists\Models;

use App\Models\Team;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class ThirdPartyAccess extends Model
{
    protected $guarded = [];
    protected $casts = ['scopes' => 'array'];

    public static function currentTeam(?string $third_party = null)
    {
        $query = static::where('team_id', Auth::user()->currentTeam->id);

        if ($third_party){
            $query->where('third_party', $third_party);
        }

        return $query->firstOrFail();
    }

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }
}
