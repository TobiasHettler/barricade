<?php

namespace Domains\Playlists\Factories;


use Domains\Auth\Models\Team;
use Domains\Auth\Models\User;
use Domains\Playlists\Models\Artist;
use Domains\Playlists\Models\Playlist;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArtistFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Artist::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'image' => $this->faker->imageUrl(),
            'spotify_id' => 'spotifyId',
            'deezer_id' => 'deezerId',
        ];
    }

    public function withSongs(): self
    {
        return $this->state(fn($attributes) => [
            'spotify_id' => 'spotify_id',
            'spotify' => [
                'id' => 'spotify_id',
                'description' => 'Spotify Description',
                'name' => $attributes['name'],
                'link' => 'dummy.de/test',
                'img' => 'pic.jpg',
                'follower_count' => 666,
                'public' => true
            ],
        ]);
    }
}
