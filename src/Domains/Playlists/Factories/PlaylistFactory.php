<?php

namespace Domains\Playlists\Factories;


use Domains\Auth\Models\Team;
use Domains\Auth\Models\User;
use Domains\Playlists\Models\Playlist;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PlaylistFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Playlist::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'public' => true,
            'spotify_id' => null,
            'deezer_id' => null,
            'spotify_description' => $this->faker->text,
            'deezer_description' => $this->faker->text,
            'spotify' => [],
            'deezer' => [],
        ];
    }

    public function withSpotify(): self
    {
        return $this->state(fn($attributes) => [
            'spotify_id' => 'spotify_id',
            'spotify' => [
                'id' => 'spotify_id',
                'description' => 'Spotify Description',
                'name' => $attributes['name'],
                'link' => 'dummy.de/test',
                'img' => 'pic.jpg',
                'follower_count' => 666,
                'public' => true
            ],
        ]);
    }

    public function withDeezer(): self
    {
        return $this->state(fn($attributes) => [
            'deezer_id' => 'deezer_id',
            'deezer' => [
                'id' => 'deezer_id',
                'description' => 'Deezer Description',
                'name' => $attributes['name'],
                'link' => 'dummy.de/test',
                'img' => 'pic.jpg',
                'follower_count' => 666,
                'public' => true
            ],
        ]);
    }
}
