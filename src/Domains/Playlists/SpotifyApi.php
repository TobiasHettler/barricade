<?php


namespace Domains\Playlists;

use Domains\Playlists\Drivers\Spotify\SpotifyConnector;
use Domains\Playlists\Models\ThirdPartyAccess;
use Illuminate\Support\Facades\Log;
use SpotifyWebAPI\SpotifyWebAPI;
use SpotifyWebAPI\SpotifyWebAPIException;

class SpotifyApi
{
    protected SpotifyWebAPI $spotifyWebApi;
    protected ThirdPartyAccess $spotifyAccess;

    public function __construct()
    {
        $this->spotifyAccess = ThirdPartyAccess::currentTeam('spotify');
        $this->spotifyWebApi = new SpotifyWebAPI();
        $this->spotifyWebApi->setAccessToken($this->spotifyAccess->access_token);
        $this->spotifyWebApi->setOptions(['return_assoc' => true]);
    }

    public function setAccessToken(string $accessToken)
    {
        $this->spotifyWebApi->setAccessToken($accessToken);
        return $this;
    }

    /**
     * Magically call the spotifyWebAPI.
     *
     * @param  string  $method
     * @param  array  $parameters
     *
     * @return mixed
     *
     * @throws \BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        if (!method_exists($this->spotifyWebApi, $method)) {
            throw new \BadMethodCallException("Method [$method] does not exist.");
        }

        try {
            return call_user_func_array([$this->spotifyWebApi, $method], $parameters);
        } catch (SpotifyWebAPIException $e) {
            try {
                (new ThirdPartyConnector('spotify'))->refresh();
                $this->setAccessToken($this->spotifyAccess->fresh()->access_token);

                return call_user_func_array([$this->spotifyWebApi, $method], $parameters);
            } catch (SpotifyWebAPIException $e) {
                Log::alert('Spotify: Token could not be refreshed ['.$e->getCode().' '.$e->getMessage().']');
            }
        }
    }
}
