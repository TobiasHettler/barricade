<?php


namespace Domains\Playlists;


use Domains\Playlists\DTOs\CreatePlaylistDTO;
use Domains\Playlists\DTOs\PlaylistData;
use Domains\Playlists\DTOs\SearchStreamingDTO;
use Domains\Playlists\DTOs\StreamingAccountData;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;
use Domains\Playlists\Exceptions\StreamingApiException;
use Domains\Playlists\Exceptions\ThirdPartyConnectorException;
use Domains\Playlists\Interfaces\StreamingApiInterface;
use JetBrains\PhpStorm\ArrayShape;

class StreamingApi
{

    protected StreamingApiInterface $apiInterface;

    public function __construct(string $driver)
    {
        $this->apiInterface = $this->createApiDriver($driver);
    }

    public static function driver(string $driver): self
    {
        return new static($driver);
    }

    public function me(): StreamingAccountData
    {
        return $this->apiInterface->me();
    }

    public function search($search)
    {
        return $this->apiInterface->search(new SearchStreamingDTO(['query' => $search]));
    }

    public function createPlaylist(CreatePlaylistDTO $data): PlaylistData
    {
        return $this->apiInterface->createPlaylist($data);
    }

    public function updatePlaylist(UpdatePlaylistOnStreamingDTO $data): PlaylistData
    {
        return $this->apiInterface->updatePlaylist($data);
    }

    protected function createApiDriver(string $driver): StreamingApiInterface
    {
        if (!in_array($driver, config('playlists.enabled'))) {
            StreamingApiException::throw('Driver ' . $driver . ' is not enabled.');
        }

        if (!array_key_exists($driver, config('playlists.drivers'))) {
            StreamingApiException::throw('Driver ' . $driver . ' does not exist.');
        }

        return new (config('playlists.drivers.' . $driver . '.api'));
    }

}
