<?php


namespace Domains\Playlists\Interfaces;


use Domains\Playlists\DTOs\CreatePlaylistDTO;
use Domains\Playlists\DTOs\PlaylistData;
use Domains\Playlists\DTOs\SearchData;
use Domains\Playlists\DTOs\SearchResultsCollection;
use Domains\Playlists\DTOs\SearchStreamingDTO;
use Domains\Playlists\DTOs\StreamingAccountData;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;

interface StreamingApiInterface
{
    public function me(): StreamingAccountData;
    public function createPlaylist(CreatePlaylistDTO $dto): PlaylistData;
    public function updatePlaylist(UpdatePlaylistOnStreamingDTO $dto): PlaylistData;
    public function search(SearchStreamingDTO $dto): SearchResultsCollection;
}
