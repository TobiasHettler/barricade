<?php

namespace Domains\Playlists\Interfaces;

use Domains\Playlists\DTOs\CreateThirdPartyAccessDTO;
use Illuminate\Http\Request;

interface ConnectorInterface
{
    public function authorizeUrl(): string;
    public function callback(Request $request): CreateThirdPartyAccessDTO;
    public function refresh(): CreateThirdPartyAccessDTO;
}
