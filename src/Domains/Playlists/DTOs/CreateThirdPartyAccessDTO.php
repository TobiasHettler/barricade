<?php


namespace Domains\Playlists\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class CreateThirdPartyAccessDTO extends DataTransferObject
{
    public string $accessToken;
    public ?string $refreshToken;
    public array $scopes;
    public string $thirdParty;
}
