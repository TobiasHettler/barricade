<?php


namespace Domains\Playlists\DTOs;


use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\DataTransferObjectCollection;

class SearchResultsCollection extends DataTransferObjectCollection
{
    public function current(): SearchData
    {
        return parent::current();
    }
}
