<?php


namespace Domains\Playlists\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class UpdatePlaylistDTO extends DataTransferObject
{
    public ?string $name;
    public ?bool $public;
    public ?string $spotify_description;
    public ?string $deezer_description;
    public ?string $spotify_id;
    public ?string $deezer_id;
}
