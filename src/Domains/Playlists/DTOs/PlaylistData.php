<?php


namespace Domains\Playlists\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class PlaylistData extends DataTransferObject
{
    public string $id;
    public string $name;
    public string $description;
    public bool $public;
    public int $follower_count;
    public ?string $img;
    public string $link;
}
