<?php


namespace Domains\Playlists\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class CreateArtistDTO extends DataTransferObject
{
    public string $name;
    public string $image;
    public ?string $spotify_id;
    public ?string $deezer_id;
}
