<?php


namespace Domains\Playlists\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class CreateSongDTO extends DataTransferObject
{
    public string $name;
    public array $artists;
    public string $duration;
    public string $image;
    public ?string $deezer_id;
    public ?string $spotify_id;
}
