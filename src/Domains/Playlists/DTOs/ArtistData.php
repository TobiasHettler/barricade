<?php


namespace Domains\Playlists\DTOs;


use Spatie\DataTransferObject\DataTransferObject;

class ArtistData extends DataTransferObject
{
    public string $name;
    public ?string $image;
    public string $id;
    public string $href;
}
