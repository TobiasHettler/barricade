<?php


namespace Domains\Playlists\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class StreamingAccountData extends DataTransferObject
{
    public string $id;
    public string $name;
    public int $followerCount;
    public string $imageHref;
    public string $link;
}
