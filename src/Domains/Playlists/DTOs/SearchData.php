<?php


namespace Domains\Playlists\DTOs;


use Spatie\DataTransferObject\DataTransferObject;

class SearchData extends DataTransferObject
{
    /**
     * Lists of types:
     *
     * @var \Domains\Playlists\DTOs\ArtistData[]
     */
    public array $artists;
    public string $name;
    public string $duration;
    public string $type;
    public string $image;
    public string $id;
}
