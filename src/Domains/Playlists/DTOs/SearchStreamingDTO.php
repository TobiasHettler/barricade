<?php


namespace Domains\Playlists\DTOs;


use Spatie\DataTransferObject\DataTransferObject;

class SearchStreamingDTO extends DataTransferObject
{
    public string $query;
}
