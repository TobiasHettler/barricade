<?php


namespace Domains\Playlists\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class CreatePlaylistDTO extends DataTransferObject
{
    public string $name;
    public bool $public;
    public string $description;
}
