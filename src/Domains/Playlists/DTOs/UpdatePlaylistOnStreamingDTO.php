<?php


namespace Domains\Playlists\DTOs;

use Spatie\DataTransferObject\DataTransferObject;

class UpdatePlaylistOnStreamingDTO extends DataTransferObject
{
    public string $id;
    public ?string $name;
    public ?bool $public;
    public ?string $description;
}
