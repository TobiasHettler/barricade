<?php


namespace Domains\Playlists\Actions;


use App\Api\Resources\PlaylistResource;
use Domains\Playlists\DTOs\CreateSongDTO;
use Domains\Playlists\DTOs\SearchResultsCollection;
use Domains\Playlists\DTOs\UpdatePlaylistDTO;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;
use Domains\Playlists\Models\Artist;
use Domains\Playlists\Models\Playlist;
use Domains\Playlists\Models\Song;
use Domains\Playlists\Resources\SearchResultsResource;
use Domains\Playlists\StreamingApi;
use Illuminate\Http\Request;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateSongAction
{
    use AsAction;

    public function handle(CreateSongDTO $dto): Song
    {
        /** @var Song $song */
        return Song::create($dto->toArray());
    }
}
