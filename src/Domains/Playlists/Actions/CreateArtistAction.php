<?php


namespace Domains\Playlists\Actions;


use App\Api\Resources\PlaylistResource;
use Domains\Playlists\DTOs\CreateArtistDTO;
use Domains\Playlists\DTOs\SearchResultsCollection;
use Domains\Playlists\DTOs\UpdatePlaylistDTO;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;
use Domains\Playlists\Models\Artist;
use Domains\Playlists\Models\Playlist;
use Domains\Playlists\Resources\SearchResultsResource;
use Domains\Playlists\StreamingApi;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateArtistAction
{
    use AsAction;

    public function handle(CreateArtistDTO $dto): Artist
    {
         return Artist::create($dto->all());
    }

}
