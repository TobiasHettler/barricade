<?php


namespace Domains\Playlists\Actions;


use Domains\Playlists\SpotifyConnector;
use Illuminate\Http\Request;
use Support\Controller;

class ConnectToSpotifyAction extends Controller
{
    public function authenticate(SpotifyConnector $spotifyConncetion)
    {
        return $spotifyConncetion->routeAuthenticate();
    }

    public function callback(Request $request, SpotifyConnector $spotifyConncetion)
    {
       $spotifyConncetion->routeCallback($request);
       return redirect(route('music'));
    }
}
