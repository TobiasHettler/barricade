<?php


namespace Domains\Playlists\Actions;


use Domains\Playlists\Models\ThirdPartyAccess;
use Domains\Playlists\SpotifyConnector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Support\Controller;

class ConnectToDeezerAction extends Controller
{
    public function authenticate()
    {
        $query = http_build_query([
            'app_id' => env('DEEZER_CLIENT_ID'),
            'redirect_uri' => 'https://950cda17b426.ngrok.io/deezer/authenticate/callback',
            'perms' => 'basic_access,manage_library,offline_access'
        ]);
        return redirect('https://connect.deezer.com/oauth/auth.php?' . $query);
    }

    public function callback(Request $request)
    {
        $response = Http::get('https://connect.deezer.com/oauth/access_token.php',[
            'app_id' => env('DEEZER_CLIENT_ID'),
            'secret' => env('DEEZER_CLIENT_SECRET'),
            'code' => $request->query('code')
        ])->body();

        parse_str($response, $result);

        ThirdPartyAccess::updateOrCreate(
            [
                'team_id' => Auth::user()->currentTeam->id,
                'third_party' => 'deezer'
            ],
            [
                'scopes' => ['basic_access','manage_library','offline_access'],
                'access_token' => $result['access_token'],
            ]
        );
        return redirect(route('music'));
    }
}
