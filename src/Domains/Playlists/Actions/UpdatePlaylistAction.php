<?php


namespace Domains\Playlists\Actions;


use App\Api\Resources\PlaylistResource;
use Domains\Playlists\DTOs\UpdatePlaylistDTO;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;
use Domains\Playlists\Models\Playlist;
use Domains\Playlists\StreamingApi;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

class UpdatePlaylistAction
{
    use AsAction;

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'spotify_description' => 'required|string',
            'deezer_description' => 'required|string',
            'spotify_id' => 'sometimes|nullable|string',
            'deezer_id' => 'sometimes|nullable|string',
            'public' => 'required|boolean',
        ];
    }

    public function handle(Playlist $playlist, UpdatePlaylistDTO $dto): Playlist
    {
         $playlist->update($dto->all());

        if ($playlist->spotify_id !== null) {
            $spotifyData = StreamingApi::driver('spotify')
                ->updatePlaylist(new UpdatePlaylistOnStreamingDTO([
                    'id' => $playlist->spotify_id,
                    'name' => $playlist->name,
                    'description' => $playlist->spotify_description,
                    'public' => $playlist->public,
                ]));

            $playlist->update([
                'spotify' => $spotifyData->all(),
                'spotify_id' => $spotifyData->id
            ]);
        }

        if ($playlist->deezer_id !== null) {
            $deezerData = StreamingApi::driver('deezer')
                ->updatePlaylist(new UpdatePlaylistOnStreamingDTO([
                    'id' => $playlist->deezer_id,
                    'name' => $playlist->name,
                    'description' => $playlist->deezer_description,
                    'public' => $playlist->public,
                ]));

            $playlist->update([
                'deezer' => $deezerData->all(),
                'deezer_id' => $deezerData->id
            ]);
        }

        return $playlist;
    }

    public function asController(ActionRequest $request, Playlist $playlist): Playlist
    {
        return $this->handle($playlist, new UpdatePlaylistDTO($request->validated()));
    }

    public function jsonResponse(Playlist $playlist)
    {
        return new PlaylistResource($playlist);
    }
}
