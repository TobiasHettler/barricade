<?php


namespace Domains\Playlists\Actions;


use App\Api\Resources\PlaylistResource;
use Carbon\Carbon;
use Domains\Playlists\DTOs\CreatePlaylistDTO;
use Domains\Playlists\DTOs\CreateSongDTO;
use Domains\Playlists\DTOs\SearchResultsCollection;
use Domains\Playlists\DTOs\StreamingAccountData;
use Domains\Playlists\DTOs\UpdatePlaylistDTO;
use Domains\Playlists\Models\Playlist;
use Domains\Playlists\Resources\SearchResultsResource;
use Domains\Playlists\StreamingApi;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;
use Support\Actions\Action;

class AddSongToPlaylistsAction
{
    use AsAction;

    public static function rules(): array
    {
        return [
            'playlists' => '',
            'song' => '',
        ];
    }

    public function handle(array $playlists, CreateSongDTO $songDTO): bool
    {
        Playlist::whereIn('id', $playlists)
            ->get()
            ->each(
                fn(Playlist $playlist) => $playlist->songs()
                    ->attach(CreateSongAction::run($songDTO))
            );

        return true;
    }


    public function asController(ActionRequest $request)
    {
        $data = $request->validated();

        return $this->handle($data['playlists'], new CreateSongDTO($data['song']));
    }

    public function jsonResponse(bool $status): JsonResponse
    {
        return response()->json(['status' => 'OK'],201);
    }
}
