<?php


namespace Domains\Playlists\Actions;

use Domains\Playlists\SpotifyApi;
use Illuminate\Http\Request;
use Support\Controller;

class UpdateSpotifyPlaylistAction extends Controller
{
    public function __invoke(Request $request)
    {
       $data =  $request->validate([
            'playlist_id' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        $spotifyApi = new SpotifyApi();

       dd($spotifyApi->updatePlaylist($data['playlist_id'], [
            'name' => $data['name'],
            'description' => $data['description'],
        ]));
    }

}
