<?php


namespace Domains\Playlists\Actions;


use Carbon\Carbon;
use Domains\Playlists\DTOs\CreatePlaylistDTO;
use Domains\Playlists\DTOs\StreamingAccountData;
use Domains\Playlists\Models\Playlist;
use Domains\Playlists\StreamingApi;
use Support\Actions\Action;

class CreatePlaylistAction extends Action
{

    public static function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'spotify_description' => 'required|string',
            'deezer_description' => 'required|string',
            'sync_spotify' => 'boolean',
            'sync_deezer' => 'boolean',
            'public' => 'boolean',
        ];
    }

    public function handle(): Playlist
    {

        $data = $this->data();
        $modelData = [
            'name' => $data['name'],
            'public' => $data['public'],
            'spotify_id' => null,
            'spotify_description' => $data['spotify_description'],
            'spotify' => [],
            'deezer_id' => null,
            'deezer_description' => $data['deezer_description'],
            'deezer' => []
        ];

        if ($data['sync_spotify'] ?? true) {
            $spotifyData = StreamingApi::driver('spotify')
                ->createPlaylist(new CreatePlaylistDTO([
                    'name' => $data['name'],
                    'description' => $data['spotify_description'],
                    'public' => $data['public'],
                ]));

            $modelData['spotify'] = $spotifyData->all();
            $modelData['spotify_id'] = $spotifyData->id;
        }


        if ($data['sync_deezer'] ?? true) {
            $deezerData = StreamingApi::driver('deezer')
                ->createPlaylist(new CreatePlaylistDTO([
                    'name' => $data['name'],
                    'description' => $data['deezer_description'],
                    'public' => $data['public'],
                ]));

            $modelData['deezer'] = $deezerData->all();
            $modelData['deezer_id'] = $deezerData->id;
        }

        return Playlist::create(
            $modelData
        );
    }
}
