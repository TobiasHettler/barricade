<?php


namespace Domains\Playlists\Actions;


use App\Api\Resources\PlaylistResource;
use Domains\Playlists\DTOs\SearchResultsCollection;
use Domains\Playlists\DTOs\UpdatePlaylistDTO;
use Domains\Playlists\DTOs\UpdatePlaylistOnStreamingDTO;
use Domains\Playlists\Models\Playlist;
use Domains\Playlists\Resources\SearchResultsResource;
use Domains\Playlists\StreamingApi;
use Illuminate\Http\Request;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

class SearchStreamingAction
{
    use AsAction;

    public function rules(): array
    {
        return [

        ];
    }

    public function handle(Request $request, string $driver): SearchResultsCollection
    {
         return StreamingApi::driver($driver)->search($request->input('q'));
    }


    public function jsonResponse(SearchResultsCollection $result)
    {
        return SearchResultsResource::collection(collect($result->items()));
    }
}
