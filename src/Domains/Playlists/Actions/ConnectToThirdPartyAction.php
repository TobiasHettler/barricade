<?php


namespace Domains\Playlists\Actions;


use Domains\Playlists\Models\ThirdPartyAccess;
use Domains\Playlists\SpotifyConnector;
use Domains\Playlists\ThirdPartyConnector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Support\Controller;

class ConnectToThirdPartyAction
{
    public static function authenticate(string $thirdParty)
    {
        return (new ThirdPartyConnector($thirdParty))
            ->startAuthentication();
    }

    public static function callback(Request $request): ThirdPartyAccess
    {
        return (new ThirdPartyConnector($request->route('thirdParty')))
            ->callback($request);
    }
}
