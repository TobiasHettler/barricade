<?php

namespace Domains\Playlists\Exceptions;


class SpotifyConnectionException extends \Exception
{
    public static function make(string $message): self
    {
        return new static($message);
    }
}
