<?php


namespace Domains\Playlists\Exceptions;

class SpotifyNotFoundException extends \Exception
{
    public static function make(string $message): self
    {
        return new static($message);
    }
}
