<?php

namespace Domains\Playlists\Exceptions;


use Support\Exceptions\Exception;

class StreamingApiException extends Exception
{
}
