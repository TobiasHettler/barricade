<?php

namespace Domains\Playlists\Exceptions;


use Support\Exceptions\Exception;

class ThirdPartyConnectorException extends Exception
{
}
