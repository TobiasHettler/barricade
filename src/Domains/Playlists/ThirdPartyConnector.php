<?php


namespace Domains\Playlists;


use Domains\Playlists\DTOs\CreateThirdPartyAccessDTO;
use Domains\Playlists\Exceptions\ThirdPartyConnectorException;
use Domains\Playlists\Interfaces\ConnectorInterface;
use Domains\Playlists\Models\ThirdPartyAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ThirdPartyConnector
{
    protected ConnectorInterface $connector;

    public function __construct(string $driver)
    {
        if (!in_array($driver, config('playlists.enabled'))){
            ThirdPartyConnectorException::throw('Driver ' . $driver . ' is not enabled.');
        }

        if (!array_key_exists($driver, config('playlists.drivers'))){
            ThirdPartyConnectorException::throw('Driver ' . $driver . ' does not exist.');
        }

        $this->driver = new (config('playlists.drivers.' . $driver .'.connector'));
    }


    public function startAuthentication(): string
    {
        return $this->driver->authorizeUrl();
    }

    public function callback(Request $request): ThirdPartyAccess
    {
        $createAccessDto = $this->driver->callback($request);
        return $this->createOrUpdateAccess($createAccessDto);
    }

    public function refresh(): ThirdPartyAccess
    {
        $createAccessDto = $this->driver->refresh();
        return $this->createOrUpdateAccess($createAccessDto);
    }

    protected function createOrUpdateAccess(CreateThirdPartyAccessDTO $dto): ThirdPartyAccess
    {
        return ThirdPartyAccess::updateOrCreate(
            [
                'team_id' => Auth::user()->currentTeam->id,
                'third_party' => $dto->thirdParty
            ],
            [
                'scopes' => $dto->scopes,
                'access_token' => $dto->accessToken,
                'refresh_token' => $dto->refreshToken,
            ]
        );
    }
}
