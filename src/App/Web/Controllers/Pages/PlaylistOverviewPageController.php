<?php

namespace App\Web\Controllers\Pages;


use App\Api\Resources\PlaylistResource;
use Domains\Playlists\Models\Playlist;
use Domains\Playlists\SpotifyApi;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Support\Controller;

class PlaylistOverviewPageController extends Controller
{
    public function __invoke(Request $request)
    {
        return Inertia::render('Playlists/Overview',[
            'playlists' => PlaylistResource::collection(Playlist::all())->resolve($request)
        ]);
    }
}
