<?php


namespace App\Web\Controllers\Pages;


use App\Api\Resources\PlaylistResource;
use Domains\Playlists\Models\Playlist;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Support\Controller;

class PlaylistController extends Controller
{
    public function edit(Playlist $playlist, Request $request)
    {
       return Inertia::render('Playlists/Edit', [
           'playlist' => PlaylistResource::make($playlist)->resolve($request)
       ]);
    }

    public function show(Playlist $playlist, Request $request)
    {
       return Inertia::render('Playlists/Show', [
           'playlist' => PlaylistResource::make($playlist->load('songs'))->resolve($request)
       ]);
    }

    public function addSong(Request $request)
    {
        return Inertia::render('Playlists/AddSong', [
            'playlists' => PlaylistResource::collection(Playlist::all())
                ->resolve($request)
        ]);
    }
}
