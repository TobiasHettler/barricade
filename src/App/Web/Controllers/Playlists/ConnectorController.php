<?php


namespace App\Web\Controllers\Playlists;


use Domains\Playlists\Actions\ConnectToThirdPartyAction;
use Illuminate\Http\Request;
use Support\Controller;

class ConnectorController extends Controller
{
    public function authenticate(string $thirdParty)
    {
        return redirect(ConnectToThirdPartyAction::authenticate($thirdParty));
    }

    public function callback(Request $request)
    {
        ConnectToThirdPartyAction::callback($request);
        return redirect(route('playlists'));
    }
}
