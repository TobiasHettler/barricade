<?php

namespace App\Providers;

use Domains\Auth\Actions\Jetstream\AddTeamMember;
use Domains\Auth\Actions\Jetstream\CreateTeam;
use Domains\Auth\Actions\Jetstream\DeleteTeam;
use Domains\Auth\Actions\Jetstream\DeleteUser;
use Domains\Auth\Actions\Jetstream\InviteTeamMember;
use Domains\Auth\Actions\Jetstream\RemoveTeamMember;
use Domains\Auth\Actions\Jetstream\UpdateTeamName;
use Domains\Auth\Models\Membership;
use Domains\Auth\Models\Team;
use Domains\Auth\Models\TeamInvitation;
use Domains\Auth\Models\User;
use Illuminate\Support\ServiceProvider;
use Laravel\Jetstream\Jetstream;

class JetstreamServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configurePermissions();

        Jetstream::useTeamModel(Team::class);
        Jetstream::useUserModel(User::class);
        Jetstream::useTeamInvitationModel(TeamInvitation::class);
        Jetstream::useMembershipModel(Membership::class);
        Jetstream::createTeamsUsing(CreateTeam::class);
        Jetstream::updateTeamNamesUsing(UpdateTeamName::class);
        Jetstream::addTeamMembersUsing(AddTeamMember::class);
        Jetstream::inviteTeamMembersUsing(InviteTeamMember::class);
        Jetstream::removeTeamMembersUsing(RemoveTeamMember::class);
        Jetstream::deleteTeamsUsing(DeleteTeam::class);
        Jetstream::deleteUsersUsing(DeleteUser::class);
    }

    /**
     * Configure the roles and permissions that are available within the application.
     *
     * @return void
     */
    protected function configurePermissions()
    {
        Jetstream::defaultApiTokenPermissions(['read']);

        Jetstream::role('admin', __('Administrator'), [
            'create',
            'read',
            'update',
            'delete',
        ])->description(__('Administrator users can perform any action.'));

        Jetstream::role('editor', __('Editor'), [
            'read',
            'create',
            'update',
        ])->description(__('Editor users have the ability to read, create, and update.'));
    }
}
