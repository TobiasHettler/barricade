<?php


namespace App\Api\Controllers;


use App\Api\Resources\StreamingAccountResource;
use Domains\Playlists\StreamingApi;
use Illuminate\Http\Request;
use Support\Controller;

class GetStreamingAccountController extends Controller
{
    public function __invoke(Request $request, string $driver)
    {
        return StreamingAccountResource::make(
            StreamingApi::driver($driver)->me()
        );
    }
}
