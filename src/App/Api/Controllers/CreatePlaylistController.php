<?php


namespace App\Api\Controllers;


use App\Api\Resources\PlaylistResource;
use Domains\Playlists\Actions\CreatePlaylistAction;
use Illuminate\Http\Request;
use Support\Controller;

class CreatePlaylistController extends Controller
{
    public function __invoke(Request $request)
    {
        $data = $request->validate(CreatePlaylistAction::rules());
        $playlist = (new CreatePlaylistAction($data))->force()->run();
        return PlaylistResource::make($playlist);
    }
}
