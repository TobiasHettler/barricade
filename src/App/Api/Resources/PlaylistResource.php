<?php


namespace App\Api\Resources;


use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaylistResource extends JsonResource
{
    public function toArray($request)
    {
//        dd( 'S',$this->relationLoaded('songs'));

        return [
            'id' => $this->id,
            'name' => $this->name,
            'public' => $this->public,
            'image' => $this->spotify['img'] ?? $this->deezer['img'] ?? null,
            'description' => $this->spotify_description,
            'songs' => SongResource::collection($this->whenLoaded('songs')),
            'spotify' => $this->when(
                !empty($this->spotify), fn() => [
                'id' => $this->spotify_id,
                'description' => $this->spotify_description,
                'name' => $this->spotify['name'],
                'link' => $this->spotify['link'],
                'img' => $this->spotify['img'] ?? null,
                'follower_count' => $this->spotify['follower_count'],
                'public' => $this->spotify['public']
            ],
                []),
            'deezer' => $this->when(!empty($this->deezer), fn() => [
                'id' => $this->deezer_id,
                'description' => $this->deezer_description,
                'name' => $this->deezer['name'],
                'link' => $this->deezer['link'],
                'img' => $this->deezer['img'],
                'follower_count' => $this->deezer['follower_count'],
                'public' => $this->deezer['public'],
            ],
                []),
        ];
    }
}
