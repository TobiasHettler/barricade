<?php


namespace App\Api\Resources;


use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StreamingAccountResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'followerCount' => $this->followerCount,
            'imageHref' => $this->imageHref,
            'link' => $this->link
        ];
 }
}
