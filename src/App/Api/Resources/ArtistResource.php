<?php


namespace App\Api\Resources;


use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArtistResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'spotify_id' => $this->spotify_id,
            'deezer_id' => $this->deezer_id,
        ];
    }
}
