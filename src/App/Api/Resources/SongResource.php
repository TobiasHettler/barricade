<?php


namespace App\Api\Resources;


use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SongResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
           // 'artists' => ArtistResource::collection($this->whenLoaded('artists')),
            'spotify_id' => $this->spotify_id,
            'deezer_id' => $this->deezer_id,
        ];
    }
}
